module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  plugins: [
    '@typescript-eslint',
  ],
  extends: [
    'plugin:@typescript-eslint/recommended',
    'airbnb',
    'airbnb-typescript',
    'airbnb/hooks',
    'airbnb/whitespace',
    'prettier',
  ],
  parserOptions: {
    project: './tsconfig.json',
  },
  rules: {
    "no-multiple-empty-lines": ["warn", { "max": 2, maxEOF: 0 }],
    "object-curly-spacing": ["error", "always"],
    "arrow-body-style": ["warn", "as-needed"],
    "no-await-in-loop": "error",
    "array-bracket-spacing": ["warn", "never"],
    "comma-spacing": ["error", { "before": false, "after": true }],
    "eol-last": ["warn", "always"],
    "no-multi-spaces": "warn",
    "indent": ["error", 2],

    "react/jsx-filename-extension": [1, { "extensions": [".jsx", ".tsx"] }],
    "react/jsx-wrap-multilines": ["error", {
      declaration: "parens-new-line",
      assignment: "parens-new-line",
      return: "parens-new-line",
      arrow: "parens-new-line",
      condition: "parens-new-line",
      logical: "parens-new-line",
      prop: "parens-new-line",
    }],

    "import/extensions": [
      "error",
      "never",
      {
        "svg": "always",
        "json": "always",
      }
    ],
    "import/first": ["warn", "absolute-first"],
    "import/no-default-export": 2,
    "import/prefer-default-export": 0,
  }
}
