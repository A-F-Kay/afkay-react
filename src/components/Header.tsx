import React, { useState } from "react";
import { Button, Classes, Navbar as BlueJSNavbar, NavbarDivider, NavbarGroup, NavbarHeading } from "@blueprintjs/core";
import { Link } from "react-router-dom";
import { TodoDialog } from "./TodoDialog";
import { APP_ROUTES } from "../constants/routes";

export function Header() {
  const [isDialogOpen, setDialogOpen] = useState(false);

  const toggleDialog = () => setDialogOpen((open) => !open);

  return (
    <BlueJSNavbar>
      <NavbarGroup>
        <NavbarHeading>A. F. Kay</NavbarHeading>
        <NavbarDivider/>
        <Link to={APP_ROUTES.home.link}>
          <Button className={Classes.MINIMAL} icon="home" text="Home"/>
        </Link>
        <Button className={Classes.MINIMAL} icon="document" text="Files"/>
      </NavbarGroup>
      <NavbarGroup align="right">
        <Button className={Classes.MINIMAL} onClick={toggleDialog} icon="notifications"/>
        <Link to={APP_ROUTES.settings.link}>
          <Button className={Classes.MINIMAL} icon="cog"/>
        </Link>
        <Button className={Classes.MINIMAL} onClick={toggleDialog} icon="user"/>
      </NavbarGroup>
      <TodoDialog isOpen={isDialogOpen} onClose={toggleDialog} />
    </BlueJSNavbar>
  )
}
