import React from "react";
import { Button, Callout, Classes, Code, Dialog, DialogProps, Intent } from "@blueprintjs/core";
import { AppTheme } from "../constants/styles";

type Props = Pick<DialogProps, "isOpen" | "onClose">;

export function TodoDialog(props: Props) {
  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <Dialog title="Feature in development" isCloseButtonShown portalClassName={AppTheme} {...props}>
      <div className={Classes.DIALOG_BODY}>
        <Callout intent={Intent.PRIMARY} icon="info-sign" title="Work In Progress">
          The component is a simple wrapper around the CSS API that provides props for modifiers and optional
          title element. Any additional HTML props will be spread to the rendered <Code>{"<div>"}</Code>{" "}
          element.
        </Callout>
      </div>
      <div className={Classes.DIALOG_FOOTER}>
        <div className={Classes.DIALOG_FOOTER_ACTIONS}>
          {/* eslint-disable-next-line react/destructuring-assignment */}
          <Button onClick={props.onClose}>Close</Button>
        </div>
      </div>
    </Dialog>
  )
}
