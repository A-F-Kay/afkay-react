import React from 'react';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { HomePage } from "./pages/HomePage";
import { Header } from "./components/Header";
import { APP_ROUTES } from "./constants/routes";
import { SettingsPage } from "./pages/SettingsPage";
import { AppTheme } from "./constants/styles";

export function App() {
  const { home, settings } = APP_ROUTES;

  return (
    <BrowserRouter>
      <div className={AppTheme} id="app_container">
        <Header/>
        <Routes>
          <Route path={home.link} element={<HomePage/>}/>
          <Route path={settings.link} element={<SettingsPage/>}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}
