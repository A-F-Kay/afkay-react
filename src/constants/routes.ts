export const APP_ROUTES = {
  home: {
    name: 'Home page',
    link: '/',
  },
  settings: {
    name: 'Settings page',
    link: '/settings'
  },
  about: {
    name: 'About page',
    link: '/about'
  },
};
